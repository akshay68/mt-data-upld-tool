# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../widget.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Widget(object):
    def setupUi(self, Widget):
        Widget.setObjectName("Widget")
        Widget.resize(475, 187)
        self.browsebtn = QtWidgets.QPushButton(Widget)
        self.browsebtn.setGeometry(QtCore.QRect(360, 70, 89, 25))
        self.browsebtn.setObjectName("browsebtn")
        self.pathtouploadfolder = QtWidgets.QLineEdit(Widget)
        self.pathtouploadfolder.setGeometry(QtCore.QRect(20, 70, 311, 25))
        self.pathtouploadfolder.setObjectName("pathtouploadfolder")
        self.procradbtn = QtWidgets.QRadioButton(Widget)
        self.procradbtn.setGeometry(QtCore.QRect(30, 120, 141, 23))
        self.procradbtn.setObjectName("procradbtn")
        self.uploadbtn = QtWidgets.QPushButton(Widget)
        self.uploadbtn.setGeometry(QtCore.QRect(270, 120, 89, 25))
        self.uploadbtn.setObjectName("uploadbtn")
        self.titlelabel = QtWidgets.QLabel(Widget)
        self.titlelabel.setGeometry(QtCore.QRect(20, 0, 251, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.titlelabel.setFont(font)
        self.titlelabel.setObjectName("titlelabel")

        self.retranslateUi(Widget)
        QtCore.QMetaObject.connectSlotsByName(Widget)

    def retranslateUi(self, Widget):
        _translate = QtCore.QCoreApplication.translate
        Widget.setWindowTitle(_translate("Widget", "Widget"))
        self.browsebtn.setText(_translate("Widget", "Browse"))
        self.procradbtn.setText(_translate("Widget", "Process the data"))
        self.uploadbtn.setText(_translate("Widget", "Upload"))
        self.titlelabel.setText(_translate("Widget", "S3 Upload  and Process Tool"))

