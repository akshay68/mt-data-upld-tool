#! /usr/bin/env python3

import boto3
import os
import sys
import CreateEc2Instance
from PyQt5 import QtCore, QtGui, QtWidgets
from UI_S3uploadandprocess import Ui_Widget

class S3uploadandprocess():
    def __init__(self):
        self.MainWidget = QtWidgets.QWidget()
        self.UIWidget = Ui_Widget()
        self.UIWidget.setupUi(self.MainWidget)
        text = "Select the directory to upload to S3"
        self.UIWidget.browsebtn.clicked.connect(lambda: self.getFolderPath(self.UIWidget.pathtouploadfolder,text))
        self.UIWidget.uploadbtn.clicked.connect(self.OnclickUpladBtn)
        self.MainWidget.show()
        
    def getFolderPath(self,LineEdit,txt):
        options = QtWidgets.QFileDialog.Options()
        #options |= QtWidgets.QFileDialog.DontUseNativeDialog
        options |= QtWidgets.QFileDialog.ShowDirsOnly
        filename = QtWidgets.QFileDialog.getExistingDirectory(None,txt,\
                                                             os.environ['HOME'],options)
        if not filename=="":
            LineEdit.setText(filename+"/")
            
    def OnclickUpladBtn(self):
        
        print("upload data to s3")
        source_dir =self.pathtouploadfolder.text()
        # read the file .rundate.txt
        if not os.path.exists(source_dir):
            return
        try:
            textfile = open(str(source_dir)+".rundate.txt",'r')
            rundate=textfile.readline()
            textfile.close()
        except IOError as e:
            print("Error readind file:%s"%e)
            print("Setting current system date")
            now = datetime.datetime.now()
            rundate = str(now.year)+'-'+str(now.month)+'-'+str(now.day)
            print(rundate)
                
        #destination = "s3://maptoddler/unprocessed_data/"+source_dir.split("/")[4]+"/"+rundate+"/"
        destination = "s3://mapbaby-raw-data/unprocessed_data/"+source_dir.split("/")[4]+"/"+rundate+"/"
        
        # Set the upload flag
        self.UploadInProgress = True
        
        UploadThread=threading.Thread(target=self.UploadDatatoS3, args=(source_dir,destination))
        UploadThread.setDaemon(False)
        UploadThread.start()
        
    def UploadDatatoS3(self,source,destination):
        
        log = open("/tmp/postprocessinglog.log","a+")
        log.write("----------------------------"+str(datetime.date.today())+"----------------------------\n")
        log.write("Source folder is "+source+"\n")
        log.write("Destination folder is "+destination+"\n")
              
        upload_cmd ="aws s3 sync --exclude='*.log' "+source+" "+destination
        
        log.write("Uploading data to S3 bucket\n")
        upload_proc = subprocess.Popen(upload_cmd, shell=True)
        log.write("Started Uploading data to S3 bucket\n")
        exit = upload_proc.wait()
        log.write("Finished uploading data to S3 bucket\n")
        
        if exit != 0:
            self.sendemail('failed', source.split('/')[len(source.split('/'))-2])
        else:
            self.sendemail('successful', source.split('/')[len(source.split('/'))-2])
             
        print("Upload finished")
                
        if self.VenueWidget.ProcessdataRDBtn.isChecked():
            # Process the folder just uploaded
            usrscript="""#!/usr/bin/env bash
                    echo "Starting user defined scripts"
                    whoami
                    sudo -H -u ubuntu bash -c "/home/ubuntu/runpostprocessingscript.sh """+ destination +" s3://mapbaby-processed-data/"+source.split('/')[len(source.split('/'))-2]+'/"'
                    
            print(usrscript)
            ec2instance = CreateEc2Instance.CreateEc2Instance(usrscript)
            
            if ec2instance.status != 0:
                log.write("["+str(datetime.datetime.now().time())+"] Eror creating / launching ec2 instance\n")
                return
            else:
                log.write("["+str(datetime.datetime.now().time())+"] Successfully created the Ec2 instance\n")
                log.write("["+str(datetime.datetime.now().time())+"] IP Address of the instance: "+str(ec2instance.instanceIP)+"\n")
                log.write("["+str(datetime.datetime.now().time())+"] Instance id: "+str(ec2instance.instanceIP)+"\n")
                
        
    def sendemail(self,status,venue):
        client = boto3.client('ses')
        if status == 'successful':
            client.send_email(\
                          Source='assets@locuslabs.com',\
                          Destination={'ToAddresses':['assets@locuslabs.com','akshay@locuslabs.com']},\
                          Message={\
                                   'Subject':{'Data':'[UPDATE]Mapbaby run-data process'},\
                                   'Body':{'Text':{'Data':'Uploading run data from '+venue+' was '+status+'.'},},\
                                   },\
                          )
        else:
            client.send_email(\
                          Source='assets@locuslabs.com',\
                          Destination={'ToAddresses':['assets@locuslabs.com','akshay@locuslabs.com']},\
                          Message={\
                                   'Subject':{'Data':'[UPDATE]Mapbaby run-data process'},\
                                   'Body':{'Text':{'Data':status+'to upload run data from '+venue+'.'},},\
                                   },\
                          )


def main():
    app = QtWidgets.QApplication(sys.argv)
    # create a object for classbrowsebtn
    ex = S3uploadandprocess()    
    app.exec_()
    sys.exit()
    
if __name__ == '__main__':
    main()

