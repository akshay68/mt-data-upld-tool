#!/usr/bin/env python
import boto3
import time

class CreateEc2Instance(object):
    
    def __init__(self,usrdata):
       
        # create an ec2 instance
        self.ec2client = boto3.client('ec2')
        self.status = -1
    
        try:
            ret = self.ec2client.run_instances(LaunchTemplate={\
    #                            'LaunchTemplateId': 'lt-0020452f415a877d1',\
                                'LaunchTemplateId': 'lt-0a72f553f1623f7e9',\
                                'Version': '18'},\
                                MaxCount=1,
                                MinCount=1,
                                UserData=usrdata,)
            self.instanceId = ret["Instances"][0]["InstanceId"]
            self.instanceIP = ret["Instances"][0]["PrivateIpAddress"]
            
            print("Instance Id:%s \nInstance IP:%s"%(self.instanceId,self.instanceIP))
    
            # check for instance status
            time.sleep(20)

            while (1):
            
                status = self.ec2client.describe_instance_status(InstanceIds=[self.instanceId])
            
                if (status['InstanceStatuses'][0]['InstanceStatus']['Status'] == 'ok') and (status['InstanceStatuses'][0]['SystemStatus']['Status'] == 'ok'):
                    print("The instance has been launched successfully and is running")
                    break
                else:
                    time.sleep(5)
                    
            self.status = 0
        except:
            print("Tried to creat a ec2 instance, but failed")
            
#    def __del__(self):
#       self.ec2client.terminate_instances(InstanceIds=[self.instanceId])
        
        

def main():
    obj = CreateEc2Instance()
    print(obj.instanceId)
    print(obj.instanceIP)        
    
if  __name__=='__main__':
    main()
